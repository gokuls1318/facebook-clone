import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { fromEventPattern } from 'rxjs';
import { HomeBodyComponent } from './home-body/home-body.component';
import { CoverPicComponent } from './cover-pic/cover-pic.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { AboutComponent } from './about/about.component';
import { CovidComponent } from './covid/covid.component';
const routes: Routes = [
  {
    path:'cover-pic',component: CoverPicComponent ,children:[
      {
        path:'timeline',component: ProfilePageComponent
      },
      {
        path:'about',component:AboutComponent
      },
      {
        path:'', redirectTo:'timeline', pathMatch:'full'
      }
    ]
    
  },

  {
    path:'', component: HomeBodyComponent
  },
  {
    path:'about', component: AboutComponent 
  },
  {
    path:'covid', component: CovidComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
