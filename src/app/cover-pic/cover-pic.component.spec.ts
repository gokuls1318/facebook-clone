import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverPicComponent } from './cover-pic.component';

describe('CoverPicComponent', () => {
  let component: CoverPicComponent;
  let fixture: ComponentFixture<CoverPicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverPicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverPicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
