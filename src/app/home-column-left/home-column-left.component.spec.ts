import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeColumnLeftComponent } from './home-column-left.component';

describe('HomeColumnLeftComponent', () => {
  let component: HomeColumnLeftComponent;
  let fixture: ComponentFixture<HomeColumnLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeColumnLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeColumnLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
