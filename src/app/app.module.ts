import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FbHeaderComponent } from './fb-header/fb-header.component';
import { HomeColumnLeftComponent } from './home-column-left/home-column-left.component';
import { HomeColumnRightComponent } from './home-column-right/home-column-right.component';
import { HomeColumnCentreComponent } from './home-column-centre/home-column-centre.component';
import { HomeBodyComponent } from './home-body/home-body.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { CoverPicComponent } from './cover-pic/cover-pic.component';
import { AboutComponent } from './about/about.component';
import { CovidComponent } from './covid/covid.component';

@NgModule({
  declarations: [
    AppComponent,
    FbHeaderComponent,
    HomeColumnLeftComponent,
    HomeColumnRightComponent,
    HomeColumnCentreComponent,
    HomeBodyComponent,
    ProfilePageComponent,
    CoverPicComponent,
    AboutComponent,
    CovidComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
