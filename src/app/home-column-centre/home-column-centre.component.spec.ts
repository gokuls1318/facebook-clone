import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeColumnCentreComponent } from './home-column-centre.component';

describe('HomeColumnCentreComponent', () => {
  let component: HomeColumnCentreComponent;
  let fixture: ComponentFixture<HomeColumnCentreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeColumnCentreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeColumnCentreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
